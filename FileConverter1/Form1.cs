﻿/* Author: Jeff Thwaites
 * Description: This is the main class of the application and is used to handle UI events and file conversion
 */

using FileConverter1.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using Converter = FileConverter1.Conversion;

namespace FileConverter1
{
    public partial class MainForm : Form
    {
        private Converter.fileType inputFileType;
        private string outputDirectory = AppDomain.CurrentDomain.BaseDirectory + "\\Converted Files\\";

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            errorLabel.Visible = false;

            //create output folder if it does not yet exist
            if(!Directory.Exists(outputDirectory))
            {
                Directory.CreateDirectory(outputDirectory);
            }
        }

        //Handles Select File button, retrieves file information, and populates the selected file/output name text boxes
        private void button1_Click(object sender, EventArgs e)
        {
            string outName = "";
            errorLabel.Visible = false;

            if (openFileDialog.ShowDialog() == DialogResult.OK && openFileDialog.CheckFileExists)
            {
                string tempName = openFileDialog.FileName.ToString();
                filePathTB.Text = tempName;

                //retrieve only the file name
                for (int i = tempName.Length - 1; i >= 0; i--)
                {
                    if (!tempName[i].Equals('\\'))//for every character that is not a '\', insert character to front of outName (this reduces work for reversing string)
                    {
                        outName = outName.Insert(0, tempName[i].ToString());
                    }
                    else //if '\' is found, escape loop
                    {
                        i = -1;
                    }
                }

                outputTB.Text = outName.Split('.')[0] + "_output";

                //repopulate extension combo box with appropriate corresponding extensions
                outputExtCombo.Items.Clear();

                //populates drop down with all possible output file extensions depending on the input files extension
                switch(outName.Split('.')[1])
                {
                    case "mp3":
                        inputFileType = Converter.fileType.mp3;
                        outputExtCombo.Items.Add(".m4a");
                        break;
                    case "m4a":
                        inputFileType = Converter.fileType.m4a;
                        outputExtCombo.Items.Add(".mp3");
                        break;
                    //case "mp4":
                    //    inputFileType = Converter.fileType.mp4;
                    //    outputExtCombo.Items.Add(".avi");
                    //    outputExtCombo.Items.Add(".mov");
                    //    outputExtCombo.Items.Add(".wmv");
                    //    break;
                    //case "avi":
                    //    inputFileType = Converter.fileType.avi;
                    //    outputExtCombo.Items.Add(".mov");
                    //    outputExtCombo.Items.Add(".mp4");
                    //    outputExtCombo.Items.Add(".wmv");
                    //    break;
                    //case "wmv":
                    //    inputFileType = Converter.fileType.wmv;
                    //    outputExtCombo.Items.Add(".avi");
                    //    outputExtCombo.Items.Add(".mov");
                    //    outputExtCombo.Items.Add(".mp4");
                    //    break;
                    //case "mov":
                    //    inputFileType = Converter.fileType.mov;
                    //    outputExtCombo.Items.Add(".avi");
                    //    outputExtCombo.Items.Add(".mp4");
                    //    outputExtCombo.Items.Add(".wmv");
                    //    break;
                    default:
                        outputTB.Clear();
                        errorLabel.Text = "Unsupported File Type";
                        errorLabel.Visible = true;
                        outputExtCombo.Items.Add("");
                        break;
                }

                outputExtCombo.SelectedIndex = 0;
                convertBtn.Enabled = !errorLabel.Visible;
            }
        }

        //Handles convert button
        private void convertBtn_Click(object sender, EventArgs e)
        {
            statusBox.Image = statusBox.InitialImage;
            statusBox.Visible = true;
            statusBox.Refresh();

            //Call necessary method depending on what type of media is getting processed (Audio or Video)
            switch (inputFileType)
            {
                case Converter.fileType.m4a:
                case Converter.fileType.mp3:
                    convertAudio();
                    break;
                case Converter.fileType.avi:
                case Converter.fileType.mov:
                case Converter.fileType.mp4:
                case Converter.fileType.wmv:
                    break;
            }
        }

        //Handles audio conversion with the use of ffmpeg
        private void convertAudio()
        {
            try
            {
                //create list of arguments for ffmpeg process
                var argument = new List<string>();

                argument.Add($"-i \"{filePathTB.Text}\"");
                argument.Add("-y");

                //add arguments that are specific to the required extension
                switch (inputFileType)
                {
                    case Conversion.fileType.m4a:
                        argument.Add("-acodec libmp3lame");
                        argument.Add("-aq 2");
                        break;
                    case Conversion.fileType.mp3:
                        argument.Add("-c:a aac");
                        argument.Add("-vn");
                        break;
                }
                argument.Add($"\"{outputDirectory}{outputTB.Text}{outputExtCombo.SelectedItem.ToString()}\"");

                //build ffmpeg process with arguments and necessary start info
                Process ffmpeg = new Process
                {
                    StartInfo =
                {
                    FileName = Path.GetDirectoryName(Directory.GetCurrentDirectory()) + "\\ffmpeg\\bin\\ffmpeg.exe",
                    Arguments = String.Join(" ", argument.ToArray()),
                    UseShellExecute = false,
                    //RedirectStandardOutput = true,
                    //RedirectStandardError = true,
                    CreateNoWindow = true
                }
                };

                ffmpeg.Start();
                //errorLabel.Visible = true;
                //errorLabel.Text = ffmpeg.StandardOutput.ReadToEnd() + "\n\n" + ffmpeg.StandardError.ReadToEnd();
                ffmpeg.WaitForExit();
                ffmpeg.Close();

                openFileDialog.Reset();

                statusBox.Image = Resources.check;

                Process.Start($@"{outputDirectory}");
            }
            catch (Exception e)
            {
                statusBox.Image = Resources.cross;
                errorLabel.Text = "Unable to process file";
                errorLabel.Visible = true;
            }
        }
    }
}
