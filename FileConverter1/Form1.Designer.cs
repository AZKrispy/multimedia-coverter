﻿namespace FileConverter1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.TitleLbl = new System.Windows.Forms.Label();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.filePathTB = new System.Windows.Forms.TextBox();
            this.outputNameLbl = new System.Windows.Forms.Label();
            this.outputTB = new System.Windows.Forms.TextBox();
            this.convertBtn = new System.Windows.Forms.Button();
            this.outputExtCombo = new System.Windows.Forms.ComboBox();
            this.errorLabel = new System.Windows.Forms.Label();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.statusBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.statusBox)).BeginInit();
            this.SuspendLayout();
            // 
            // TitleLbl
            // 
            this.TitleLbl.AutoSize = true;
            this.TitleLbl.BackColor = System.Drawing.Color.Transparent;
            this.TitleLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TitleLbl.ForeColor = System.Drawing.Color.White;
            this.TitleLbl.Location = new System.Drawing.Point(176, 9);
            this.TitleLbl.Name = "TitleLbl";
            this.TitleLbl.Size = new System.Drawing.Size(172, 25);
            this.TitleLbl.TabIndex = 0;
            this.TitleLbl.Text = "Media Converter";
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(33, 68);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "&Select File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // filePathTB
            // 
            this.filePathTB.Enabled = false;
            this.filePathTB.Location = new System.Drawing.Point(114, 70);
            this.filePathTB.Name = "filePathTB";
            this.filePathTB.ReadOnly = true;
            this.filePathTB.Size = new System.Drawing.Size(344, 20);
            this.filePathTB.TabIndex = 1;
            // 
            // outputNameLbl
            // 
            this.outputNameLbl.AutoSize = true;
            this.outputNameLbl.BackColor = System.Drawing.Color.Transparent;
            this.outputNameLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outputNameLbl.ForeColor = System.Drawing.Color.White;
            this.outputNameLbl.Location = new System.Drawing.Point(12, 119);
            this.outputNameLbl.Name = "outputNameLbl";
            this.outputNameLbl.Size = new System.Drawing.Size(96, 17);
            this.outputNameLbl.TabIndex = 4;
            this.outputNameLbl.Text = "Output Name:";
            // 
            // outputTB
            // 
            this.outputTB.Location = new System.Drawing.Point(114, 119);
            this.outputTB.Name = "outputTB";
            this.outputTB.Size = new System.Drawing.Size(138, 20);
            this.outputTB.TabIndex = 1;
            // 
            // convertBtn
            // 
            this.convertBtn.Location = new System.Drawing.Point(114, 145);
            this.convertBtn.Name = "convertBtn";
            this.convertBtn.Size = new System.Drawing.Size(75, 23);
            this.convertBtn.TabIndex = 3;
            this.convertBtn.Text = "&Convert";
            this.convertBtn.UseVisualStyleBackColor = true;
            this.convertBtn.Click += new System.EventHandler(this.convertBtn_Click);
            // 
            // outputExtCombo
            // 
            this.outputExtCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.outputExtCombo.FormattingEnabled = true;
            this.outputExtCombo.Location = new System.Drawing.Point(258, 118);
            this.outputExtCombo.Name = "outputExtCombo";
            this.outputExtCombo.Size = new System.Drawing.Size(48, 21);
            this.outputExtCombo.TabIndex = 2;
            // 
            // errorLabel
            // 
            this.errorLabel.AutoSize = true;
            this.errorLabel.BackColor = System.Drawing.Color.Transparent;
            this.errorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorLabel.ForeColor = System.Drawing.Color.Red;
            this.errorLabel.Location = new System.Drawing.Point(111, 93);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(151, 15);
            this.errorLabel.TabIndex = 9;
            this.errorLabel.Text = "Unsupported File Type";
            // 
            // statusBox
            // 
            this.statusBox.BackColor = System.Drawing.Color.Transparent;
            this.statusBox.ErrorImage = null;
            this.statusBox.Image = global::FileConverter1.Properties.Resources.loading;
            this.statusBox.ImageLocation = "";
            this.statusBox.InitialImage = global::FileConverter1.Properties.Resources.loading;
            this.statusBox.Location = new System.Drawing.Point(195, 145);
            this.statusBox.Name = "statusBox";
            this.statusBox.Size = new System.Drawing.Size(25, 25);
            this.statusBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.statusBox.TabIndex = 10;
            this.statusBox.TabStop = false;
            this.statusBox.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGray;
            this.BackgroundImage = global::FileConverter1.Properties.Resources.background;
            this.ClientSize = new System.Drawing.Size(504, 186);
            this.Controls.Add(this.statusBox);
            this.Controls.Add(this.errorLabel);
            this.Controls.Add(this.outputExtCombo);
            this.Controls.Add(this.convertBtn);
            this.Controls.Add(this.outputTB);
            this.Controls.Add(this.outputNameLbl);
            this.Controls.Add(this.filePathTB);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TitleLbl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Media Converter";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.statusBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label TitleLbl;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox filePathTB;
        private System.Windows.Forms.Label outputNameLbl;
        private System.Windows.Forms.TextBox outputTB;
        private System.Windows.Forms.Button convertBtn;
        private System.Windows.Forms.ComboBox outputExtCombo;
        private System.Windows.Forms.Label errorLabel;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.PictureBox statusBox;
    }
}

